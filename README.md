###Sample JDBC - using Swing###

created a sample database name mydb
that stores a listing of medical term
that are associated with specific diagnosis.

Connect to the Database using the JDBC API and created
the GUI using Swing AWT framework.

Database used in demo is JavaDB

The following screen shot shows the interaction with
the database and I have also included a distributed version
in of the Java Application in JAR files.